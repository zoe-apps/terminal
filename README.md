# Workspace terminal app

This ZApp will start a browser-based terminal that you can use to interact with your workspace using shell commands.

Currently the [xTerm.js](https://github.com/xtermjs/xterm.js) is used with a TTYd backend.

